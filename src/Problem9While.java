import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n ;
        int i = 1 ;
        System.out.print("Please input n: ");
        n = sc.nextInt();
        while (i<= n) {
            System.out.println("");
            i++;
            int j = 1 ;
            while (j<=n) {
                System.out.print(j);
                j++;
            }
        }
        sc.close();    
    }
}
